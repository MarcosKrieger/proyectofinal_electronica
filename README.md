# Proyecto Final Ingeniería Electrónica

## Módulo de adquisición y procesamiento de datos provenientes de celdas de carga

Este proyecto se realizo para una empresa del sector agropecuario de Argentina. Es un prototipo el cual buscar analizar la viabilidad de remplazar una báscula comercial utilizada para el pesaje de camiones con carga utilizando insumos de bajo costo, modernos y que se consigan con facilidad en la industria local. 

Desarrollo de un módulo electrónico que sea capaz de:

    *  Alcanzar la mayor precisión posible en la medición.  
    *  Almacenar y registrar las mediciones (backup).
    *  Lograr estabilidad en las mediciones dadas.
    *  Facilitar el acceso a visualización de las mediciones. 
    *  Interactuar con el módulo mediante una aplicación móvil.
    *  Ser portable y con alimentación interna.
    *  Fabricado con insumos disponibles en el país.
    *  Conseguir una fácil conexión a diferentes celdas de carga.
    *  Brindar un sistema de registro con fecha y hora.
    

